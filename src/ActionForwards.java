import java.util.ArrayList;

//Класс служжит для передачи информации об объекте, который
//участвует в бою
public class ActionForwards {
    public int id_other_object, id_my, id_my_tank;
    public int x,y;
    public int inx;
    public boolean result;
    public TanksPlayer.EN_TRENDS trend;
    //Массив ячеек из которых состоит объект
    ArrayList<Points> points=new ArrayList<Points>();
    //Стереть данные
    public void clear()
    {
        id_other_object=-1;
        id_my=-1;
        id_my_tank=-1;
        result=false;
        x=0;y=0;
        points.clear();
    }
    //Конструктор
    ActionForwards()
    {
        id_other_object=-1;
        id_my=0;
        x=0;y=0;
        result=false;
        id_my_tank=-1;
        trend= TanksPlayer.EN_TRENDS.E_UP;
    }
    ActionForwards(int Id_My,int X,int Y)
    {
        id_other_object=-1;
        id_my=Id_My;
        x=X;y=Y;
        result=false;
        trend= TanksPlayer.EN_TRENDS.E_UP;
    }
    //Установить точки танка
    public void setPoints(int x,int y)
    {
        points.clear();
        points.add(new Points(x-1,y-1));
        points.add(new Points(x,y-1));
        points.add(new Points(x+1,y-1));
        points.add(new Points(x-1,y));
        points.add(new Points(x,y));
        points.add(new Points(x+1,y));
        points.add(new Points(x-1,y+1));
        points.add(new Points(x,y+1));
        points.add(new Points(x+1,y+1));
    }
    //Установить точку одного объекта
    public void setOnePonit(int x,int y)
    {
        points.clear();
        points.add(new Points(x,y));
    }
    //Класс Точки
    class Points {
        int X,Y;
        Points(int x, int y){
            X=x;Y=y;
        }
    }
}
