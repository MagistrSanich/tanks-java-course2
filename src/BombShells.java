import java.awt.*;

//Снаряд танка
public class BombShells {

    int x_center,y_center;
    private TankMachine.EN_TRENDS trend;
    private int size_cell;
    int id,id_my_tank;
    EN_TYPE_TANK type_tank;
    ActionForwards my_action;
    int color_red,color_green,color_blue;

    //Тип танка
    enum EN_TYPE_TANK
    {
        EN_EVIL, //Танк бота
        EN_GOOD  //Танк Игрока
    }
    public BombShells(int X_Center, int Y_Center, int SizeOfSell, TankMachine.EN_TRENDS Trend,
                      int ID, int ID_OF_TANK,EN_TYPE_TANK Type_Of_Tank){
        x_center=X_Center;
        y_center=Y_Center;
        size_cell=SizeOfSell;
        id=ID;
        id_my_tank=ID_OF_TANK;
        type_tank=Type_Of_Tank;

        trend = Trend;
        my_action=new ActionForwards(id,x_center,y_center);
        this.SetColor(255,0,0);
    }
    //Задать цвет
    public void SetColor(int red, int green, int blue){
        color_red=red;
        color_green=green;
        color_blue = blue;
    }
    //Нарисовать снаряд
    public void Print(Graphics g){
        g.setColor(new Color(color_red,color_green,color_blue));
        g.fillRect(x_center*size_cell,y_center*size_cell,size_cell,size_cell);;
    }
    //Рассчитать траекторию движения
    public void chekMove(ActionForwards action_bf)
    {
        my_action.x=x_center;
        my_action.y=y_center;
        final int DIST =1;
        if (trend== TankMachine.EN_TRENDS.E_RIGHT) {
            my_action.x = x_center + DIST;
            my_action.y = y_center;
        }
        if (trend == TankMachine.EN_TRENDS.E_UP)
        {
            my_action.x=x_center;
            my_action.y=y_center - DIST;
        }
        if (trend== TankMachine.EN_TRENDS.E_LEFT)
        {
            my_action.x=x_center-DIST;
            my_action.y=y_center;
        }
        if (trend== TankMachine.EN_TRENDS.E_DOWN)
        {
            my_action.x=x_center;
            my_action.y=y_center + DIST;
        }
        action_bf.x=my_action.x;
        action_bf.y=my_action.y;
        action_bf.setOnePonit(my_action.x,my_action.y);
        action_bf.id_my=id;
        action_bf.id_my_tank=id_my_tank;
        action_bf.id_other_object=-1;
    }
    //Переместиться в заданную точку
    public void releseMove(boolean flag)
    {
        if(flag)
        {
            x_center=my_action.x;
            y_center=my_action.y;
        }
    }
    //Проверить не было ли столкновения
    public boolean checkHit(ActionForwards action) {
        for (int i = 0; i < action.points.size(); i++) {
            int xx = x_center;
            int yy = y_center;
            int fx = action.points.get(i).X;
            int fy = action.points.get(i).Y;
            //6 cells
            if (fx == xx && fy == yy)
                return true;
            if (fx == xx && fy == yy)
                return true;
            if (fx == xx && fy == yy)
                return true;
            if (fx == xx && fy == yy)
                return true;
        }
        return false;
    }

}