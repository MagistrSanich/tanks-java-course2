//Класс нужный для передаче информации с поля боя
enum EN_STATUS_WAR {
    STATUS_NOTHING,//Бой продолжается
    STATUS_FAIL,//Игрок проиграл
    STATUS_VICTORY//Игрок победил
}
public class EventsInTheWar {
    public EN_STATUS_WAR status_war = EN_STATUS_WAR.STATUS_NOTHING;
}
