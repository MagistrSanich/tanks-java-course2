import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyAdapter;
import java.util.ArrayList;

public class Main  extends JPanel implements ActionListener {
    static final int DISPLAY_WIDTH =900,MENU_WIDTH=400;
    static final int DISPLAY_HEIGHT =900,MENU_HEIGHT=350;
    final int FPS = 20;
    static ENUM_WORK_MODE work_mode=ENUM_WORK_MODE.E_PLAY;
    int level=1;
    final int LAST_LEVEL=3;
    final int COUNT_OF_CELLS=50;
    final int SIZE_OF_CELLS = DISPLAY_WIDTH/COUNT_OF_CELLS;
    static int morders;
    static int death;
    static JFrame f;

    //Battlefield's components
    final int TIME_RELOAD=1500,RATE_OF_FIRE=500;//мс
    int id_count;
    static int count_lives;
    static int count_tanks_spawn;
    boolean start_game;
    boolean pause;
    EventsInTheWar event_war;
    ActionForwards action_bf;
    ArrayList<TanksBot> allBots=new ArrayList<TanksBot>();
    ArrayList<TanksPlayer>players=new ArrayList<TanksPlayer>();
    ArrayList<BombShells>bombShells=new ArrayList<BombShells>();
    ArrayList<Stones>allStones=new ArrayList<Stones>();
    ArrayList<Stones>weakStones=new ArrayList<Stones>();
    ArrayList<Spawns>spawns= new ArrayList<Spawns>();

    //Таймер
    Timer timer = new Timer(1000/FPS,this);
    //Buttons
    private JButton button_continue = new JButton("Continue");
    private JButton button_exit = new JButton("Exit");

    //Режим игры
    enum ENUM_WORK_MODE
    {
        E_MENU,
        E_PLAY,
        E_GO_OUT
    }
    //Конструктор главного класса
    public Main()
    {
        //Battlefield:
        id_count=2;
        action_bf = new ActionForwards();
        start_game=false;
        pause=false;
        count_tanks_spawn=10;
        work_mode=ENUM_WORK_MODE.E_MENU;

        //Game
        timer.start();
        addKeyListener(new KeyBoard());

        //Buttons
        button_continue.setBounds((DISPLAY_WIDTH-190)/2,(DISPLAY_HEIGHT-190)/2,200,100);
        button_continue.addActionListener(new ButtonEventListener());
        button_continue.setVisible(true);
        button_continue.setFocusable(false);

        button_exit.setBounds((DISPLAY_WIDTH-190)/2,(DISPLAY_HEIGHT-190)/2+100+15,200,100);
        button_exit.addActionListener(new ButtonEventListener2());
        button_exit.setVisible(true);
        button_exit.setFocusable(false);
        setFocusable(true);
    }
    //Отрисовка всех графических объектов + регулировка игры
    public void paint(Graphics g)
    {
        if(work_mode==ENUM_WORK_MODE.E_PLAY) {
            g.clearRect(0,0,DISPLAY_WIDTH,DISPLAY_HEIGHT);
            g.setColor(new Color(0, 0, 0));
            g.fillRect(0, 0, DISPLAY_WIDTH, DISPLAY_HEIGHT);

            g.setColor(new Color(155, 155, 155));
            for (int i = 0; i < DISPLAY_WIDTH; i += SIZE_OF_CELLS) {
                g.drawLine(i, 0, i, DISPLAY_HEIGHT);
            }
            for (int j = 0; j < DISPLAY_HEIGHT; j += SIZE_OF_CELLS) {
                g.drawLine(0, j, DISPLAY_WIDTH, j);
            }
            for (int i = 0; i < players.size(); i++) {
                players.get(i).Print(g);
            }
            for (int i = 0; i < allBots.size(); i++) {
                allBots.get(i).Print(g);
            }
            for (int i = 0; i < allStones.size(); i++) {
                allStones.get(i).Print(g);
            }
            for (int i = 0; i < weakStones.size(); i++) {
                weakStones.get(i).Print(g);
            }
            for (int i = 0; i < bombShells.size(); i++) {
                bombShells.get(i).Print(g);
            }
        } else if(work_mode==ENUM_WORK_MODE.E_MENU)
        {
            pause=true;
            g.clearRect(0,0,DISPLAY_WIDTH,DISPLAY_HEIGHT);
            timer.stop();
            if(start_game==true)
            {
                f.add(button_continue);
                button_continue.setSelected(false);
                button_continue.setSelected(true);
                f.add(button_exit);
                button_exit.setSelected(false);
                button_exit.setSelected(true);
            }

        } else if(work_mode==ENUM_WORK_MODE.E_GO_OUT)
        {

        }


    }
    //Исполняемый метод
    public static void main(String[] args) {
        //Создание оконнго интерфейса
        f = new JFrame("Kursach");
        f.setSize(DISPLAY_WIDTH,DISPLAY_HEIGHT);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.setLayout(new BorderLayout(1,1));
        f.setResizable(false);
        f.setLocationRelativeTo(null);
        f.add(new Main());

        //Create and start the menu
        MenuGUI app = new MenuGUI(MENU_WIDTH,MENU_HEIGHT);
        app.setVisible(true);

    }
    //Действия таймера
    @Override
    public void actionPerformed(ActionEvent e) {
        this.gamePlay();
        repaint();
    }
    //Действия после нажатие клавиш
    public class KeyBoard extends KeyAdapter
    {
        public void keyPressed(KeyEvent event)
        {
            int key = event.getKeyCode();
            for(int i=0;i<players.size();i++)
            {
                players.get(i).reedKeyBoard(key);
            }
            if(key==KeyEvent.VK_ESCAPE)work_mode=ENUM_WORK_MODE.E_MENU;
        }
        public void keyReleased(KeyEvent e) {
            int key = e.getKeyCode();
            for(int i=0;i<players.size();i++)
            {
                players.get(i).setTrendStand(key);
            }
        }
    }
    //Рассчет движения и взаимодействия танков, стен, камней, снарядов
    public void CulculateAll(EventsInTheWar event_in_the_war) {
        //Move
        for(int i=players.size()-1;i>=0;i--)
        {
            action_bf.clear();
            players.get(i).chekMove(action_bf);
            players.get(i).releseMove(!this.checkBarriers(action_bf));
            if(action_bf.result)
            {
                if(players.get(i).getLives()>1)
                {
                    players.get(i).decLives();
                } else
                    players.remove(i);
                this.punishBomb(action_bf.id_other_object);
            } else
            if(players.get(i).getShoot() )
            {
                bombShells.add(new BombShells(action_bf.x,action_bf.y,SIZE_OF_CELLS,
                        players.get(i).getTrend(),getId(),action_bf.id_my,BombShells.EN_TYPE_TANK.EN_GOOD));
                players.get(i).setShoot(false);
            }
        }
        for(int i=allBots.size()-1;i>=0;i--)
        {
            action_bf.clear();
            allBots.get(i).chekMove(action_bf);
            allBots.get(i).releseMove(!this.checkBarriers(action_bf));
            if(action_bf.result)
            {
                if(bombShells.get(action_bf.inx).type_tank!=BombShells.EN_TYPE_TANK.EN_EVIL)
                    allBots.remove(i);
                this.punishBomb(action_bf.id_other_object);

            } else
            if(allBots.get(i).getShoot())
            {
                bombShells.add(new BombShells(action_bf.x,action_bf.y,SIZE_OF_CELLS,
                        allBots.get(i).getTrend(),getId(),action_bf.id_my,BombShells.EN_TYPE_TANK.EN_EVIL));
                allBots.get(i).setShoot(false);
            }
        }
        for(int i=bombShells.size()-1;i>=0;i--)
        {
            for(int j=0;j<2;j++) {
                action_bf.clear();
                bombShells.get(i).chekMove(action_bf);
                bombShells.get(i).releseMove(!this.checkBarriers(action_bf));
                if (action_bf.result) {
                    bombShells.remove(i);
                    this.punishBomb(action_bf.id_other_object);
                    i--;
                    break;
                } else if (action_bf.id_other_object > 0) {
                    this.punish(action_bf.id_other_object,bombShells.get(i).type_tank);
                    bombShells.remove(i);
                    break;
                }
            }
        }
        for(int i=spawns.size()-1;i>=0;i--)
        {
            action_bf.clear();
            action_bf.setPoints(spawns.get(i).x_center,spawns.get(i).y_center);
            if(!this.checkBarriers(action_bf))
            {
                if(spawns.get(i).createTank())
                {
                    this.addBot(spawns.get(i).x_center,spawns.get(i).y_center);
                }
            }

        }
        boolean end_of_tanks=true;
        for(int i=0;i<spawns.size();i++)
            if(spawns.get(i).count_tanks>0) {
                end_of_tanks=false;
                break;
            }
        if(players.size()<=0) event_in_the_war.status_war=EN_STATUS_WAR.STATUS_FAIL;
        else if(end_of_tanks && allBots.size()<=0)event_in_the_war.status_war=EN_STATUS_WAR.STATUS_VICTORY;
        else event_in_the_war.status_war=EN_STATUS_WAR.STATUS_NOTHING;

    }
    //Организация самой игры
    private void gamePlay() {
        switch (work_mode) {
            case E_MENU: {
                break;
            }
            case E_PLAY: {
                if (pause) {
                    f.remove(button_exit);
                    f.remove(button_continue);
                    pause = false;
                }
                if (!start_game) {
                    //Create messanger
                    event_war = new EventsInTheWar();
                    //Init levels
                    this.createLevel(level);
                    start_game = true;
                }
                this.CulculateAll(event_war);
                if (event_war.status_war == EN_STATUS_WAR.STATUS_FAIL) {
                    work_mode = ENUM_WORK_MODE.E_MENU;
                    level = 1;
                    this.createLevel(level);
                    event_war.status_war = EN_STATUS_WAR.STATUS_NOTHING;

                    break;
                } else if (event_war.status_war == EN_STATUS_WAR.STATUS_VICTORY) {
                    level++;
                    if (level > LAST_LEVEL) {
                        work_mode = ENUM_WORK_MODE.E_MENU;
                        level = 1;
                        this.createLevel(level);
                        event_war.status_war = EN_STATUS_WAR.STATUS_NOTHING;
                        break;
                    }
                    event_war.status_war = EN_STATUS_WAR.STATUS_NOTHING;
                    this.createLevel(level);
                }
                break;
            }
            case E_GO_OUT: {
                timer.stop();
                showStatistics();
                break;
            }
        }

    }
    //Показать статистику за игру
    void showStatistics()
    {

        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        int x=dim.width/2-MENU_WIDTH/2;
        int y=dim.height/2-MENU_HEIGHT/2;
        StatisticsGUI statApp = new StatisticsGUI(x,y,MENU_WIDTH,MENU_HEIGHT);
        statApp.setVisible(true);
    }
    //Создать новый уровень
    void createLevel(int number_level)
    {
        switch (number_level)
        {
            case 1:
            {
                this.clear();
                int xx=100,yy=100,d=55;
                for(int i=2;i<2+3*1;i+=3){
                    //this.addBot(i,2);
                    this.addBot(i,25);
                }
                this.addPlayer(33,3);
                yy=3;
                for(xx=4;xx<18;xx++)
                    this.addWeakObject(xx,yy);
                this.addWeakObject(2,2);
                this.createJail();
                break;
            }
            case 2:
            {
                this.clear();
                this.createJail();

                int xx = COUNT_OF_CELLS;
                int yy = DISPLAY_HEIGHT/SIZE_OF_CELLS;
                for(int i=1;i<50;i++){
                    for(int j=0;j<8;j++)
                        this.addWeakObject(j+35,i);
                }
                this.addPlayer(5,yy-10);
                this.addSpawn(10,10,1000);
                this.addSpawn(20,10,1000);
                this.addSpawn(30,10,1000);

                break;
            }
            case 3:
            {
                this.clear();
                this.createJail();

                int xx = COUNT_OF_CELLS;
                int yy = DISPLAY_HEIGHT/SIZE_OF_CELLS/2;
                for(int i=1;i<xx;i++){
                    for(int j=0;j<8;j++)
                        this.addWeakObject(i,yy/2+j);
                }
                for(int i=1;i<yy;i++)
                    for(int j=0;j<8;j++)
                        this.addWeakObject(xx/2/2-4+j,i);
                this.addPlayer(5,yy-10);
                this.addSpawn(10,10,1000);
                this.addSpawn(xx/2-10,10,1000);
                this.addSpawn(xx/2-10,yy-10,1000);
                break;
            }
        }
    }
    //Очистить уровень
    public void clear()
    {
        allBots.clear();
        players.clear();
        bombShells.clear();
        allStones.clear();
        weakStones.clear();
        spawns.clear();
    }
    //Создать неразрущаемые стенки по краям поля
    public void createJail()
    {
        for (int i=0;i<DISPLAY_HEIGHT/SIZE_OF_CELLS;i++)
        {
            this.addStone(0,i);
            this.addStone(DISPLAY_WIDTH/SIZE_OF_CELLS-1,i);
        }
        for(int i=0;i<DISPLAY_WIDTH/SIZE_OF_CELLS;i++)
        {
            this.addStone(i,0);
            this.addStone(i,DISPLAY_HEIGHT/SIZE_OF_CELLS-1);
        }
    }
    //Уничтожить объект в который врезался снаряд
    public void punish(int id_unit, BombShells.EN_TYPE_TANK type_tank)
    {
        for(int i=0;i<players.size();i++)
        {
            if(players.get(i).id==id_unit)
            {
                if(players.get(i).getLives()>1)
                {
                    players.get(i).decLives();
                } else players.remove(i);
                death++;
                return;
            }
        }
        if(type_tank!=BombShells.EN_TYPE_TANK.EN_EVIL) {
            for (int i = 0; i < allBots.size(); i++) {
                if (allBots.get(i).id == id_unit) {
                    allBots.remove(i);
                    morders++;
                    return;
                }
            }
        }
        for(int i=0;i<weakStones.size();i++)
        {
            if(weakStones.get(i).id_obj == id_unit)
            {
                weakStones.remove(i);
                return;
            }
        }
    }
    //Уничтожить снаряд
    public void punishBomb(int id_unit)
    {
        for(int i=0;i<bombShells.size();i++)
        {
            if(bombShells.get(i).id==id_unit)
            {
                bombShells.remove(i);
                return;
            }
        }
    }
    //Проерка объектов на столкновение
    public boolean checkBarriers(ActionForwards action)
    {
        action.result=false;

        for(int i=0;i<players.size();i++)
        {
            if(action.id_my==players.get(i).id || action.id_my_tank==players.get(i).id) continue;
            if(players.get(i).checkHit(action))
            {
                action.id_other_object=players.get(i).id;
                action.inx=i;
                return true;
            }
        }

        for(int i=0;i<allBots.size();i++)
        {
            if(action.id_my==allBots.get(i).id || action.id_my_tank==allBots.get(i).id) continue;
            if(allBots.get(i).checkHit(action))
            {
                action.id_other_object=allBots.get(i).id;
                action.inx=i;
                return true;
            }
        }

        for(int i=0;i<allStones.size();i++)
        {
            if(allStones.get(i).checkHit(action))
            {
                action.id_other_object=allStones.get(i).id_obj;
                action.inx=i;
                return true;
            }
        }
        for(int i=0;i<weakStones.size();i++)
        {
            if(weakStones.get(i).checkHit(action))
            {
                action.id_other_object=weakStones.get(i).id_obj;
                action.inx=i;
                return true;
            }
        }

        for(int i=0;i<bombShells.size();i++) {

            if (bombShells.get(i).id_my_tank == action.id_my || bombShells.get(i).id ==action_bf.id_my) continue;
            if (bombShells.get(i).checkHit(action)) {
                action.id_other_object = bombShells.get(i).id;
                action.result = true;
                action.inx=i;
                return true;
            }
        }
        return false;
    }
    //Добавить Бота
    public void addBot(int xx, int yy){
        allBots.add(new TanksBot(xx,yy,SIZE_OF_CELLS,TIME_RELOAD,getId(),RATE_OF_FIRE));
    }
    //Добавить игрока
    public void addPlayer(int xx, int yy){
        players.add(new TanksPlayer(xx,yy,SIZE_OF_CELLS,TIME_RELOAD,getId(),count_lives));
    }
    //Добавить неразрушаемый объект
    public void addStone(int xx,int yy){
        allStones.add(new Stones(xx,yy,SIZE_OF_CELLS,getId()));
    }
    //Добавить разрушаемый объект
    public void addWeakObject(int xx,int yy){
        weakStones.add(new Stones(xx,yy,SIZE_OF_CELLS,getId()));
    }
    //Добавить создатель танков (спаун)
    public void addSpawn(int X_Center,int Y_Center,int Timer)
    {
        spawns.add(new Spawns(X_Center,Y_Center,count_tanks_spawn,Timer));
    }
    //Сгенерировать id объекта
    public int getId()
    {
        id_count++;
        return id_count;
    }
    //Класс для прослушивания кнопки "Продолжить"
    class ButtonEventListener implements ActionListener
    {
        //Продолжить игру
        public void actionPerformed(ActionEvent e)
        {
            work_mode=ENUM_WORK_MODE.E_PLAY;
            timer.start();
        }
    }
    //Класс для прослушивания кноаки "Выход"
    class ButtonEventListener2 implements ActionListener
    {
        //Выйти
        public void actionPerformed(ActionEvent e)
        {
            timer.start();
            work_mode=ENUM_WORK_MODE.E_GO_OUT;
            f.setVisible(false);
            f.dispose();


        }
    }
}
