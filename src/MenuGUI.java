
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

//Меню перед началом игры
public class MenuGUI extends JFrame
{
    private JButton button = new JButton("Start");
    private JLabel label_title = new JLabel("Welcome to the game developing intellectual abilities");
    private JLabel label_t_s = new JLabel("Count tanks in Spawns(<100): ");
    private JLabel label_l_p = new JLabel("Count livies players(<100): : ");
    private JTextField input_t_s = new JTextField("10");
    private JTextField input_l_p = new JTextField("1");

    private int count_t_p;
    private int count_l_p;

    //Конструктор
    public MenuGUI( int Width, int Height)
    {
        super("Kursach: Tanks");
        this.setLayout(new BorderLayout(1,1));
        this.setSize(Width,Height);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Container container = this.getContentPane();
        input_t_s.addFocusListener(new InputEventListenerSpawn());
        input_l_p.addFocusListener(new InputEventListenerLives());

        final int INDENT=5;
        int width=Width/2-INDENT*2,height=50;
        int d_h=50,d_w=Width/2+2*INDENT,
                x=0+INDENT,y=10;
        container.setLayout(null);
        //Title
        container.add(label_title);
        label_title.setBounds(x,y,Width-INDENT/2,height);

        //Tanks in Spawn
        y+=d_h;
        container.add(label_t_s);
        label_t_s.setBounds(x,y,width,height);

        container.add(input_t_s);
        input_t_s.setBounds(x+d_w,y,width-6*INDENT,height);

        //Count livies
        y+=d_h;
        container.add(label_l_p);
        label_l_p.setBounds(x,y,width,height);

        container.add(input_l_p);
        input_l_p.setBounds(x+d_w,y,width-6*INDENT,height);

        //Button
        x=Width/2-width/2;
        width=Width/2;
        height=80;
        y+=2*d_h;
        button.addActionListener(new ButtonEventListener());
        container.add(button);
        button.setBounds(x,y,width,height);
        setResizable(false);

    }
    //Классы для компонент
    class ButtonEventListener implements ActionListener
    {
        //Действия в случае нажатия кнопки старт
        public void actionPerformed(ActionEvent e)
        {
            if(!(input_l_p.getBackground()==Color.RED || input_t_s.getBackground()==Color.RED)) {
                setVisible(false);
                dispose();
                Main.f.setVisible(true);
                Main.work_mode= Main.ENUM_WORK_MODE.E_PLAY;
                Main.count_tanks_spawn=count_t_p;
                Main.count_lives=count_l_p;
            } else {
                String message = "Error! Incorrect data entered!" ;
                JOptionPane.showMessageDialog(null,
                        message,"Output",JOptionPane.PLAIN_MESSAGE);
            }
        }
    }
    //Класс для поля ввода
    class InputEventListenerSpawn implements FocusListener
    {
        //Если начали вводить значение
        public void focusGained(FocusEvent e) { }
        //Если закончили вводить значение
        public void focusLost(FocusEvent e) {
            try {
                count_t_p = Integer.parseInt(input_t_s.getText());
                if(!(count_t_p>1 && count_t_p<100))
                {
                    input_t_s.setBackground(Color.RED);
                } else
                    input_t_s.setBackground(Color.WHITE);
            }catch (NumberFormatException ex)
            {
                System.out.println("Error: "+ex.getMessage());
                input_t_s.setBackground(Color.RED);
            }
        }

    }
    //Класс для поля ввода
    class InputEventListenerLives implements FocusListener
    {
        //Если начали вводить значение
        public void focusGained(FocusEvent e) { }
        //Если закончили вводить значение
        public void focusLost(FocusEvent e) {
            try {
                count_l_p = Integer.parseInt(input_l_p.getText());
                if (!(count_l_p>0 && count_l_p<100))
                {
                    input_l_p.setBackground(Color.RED);
                }else
                    input_l_p.setBackground(Color.WHITE);
            }catch (NumberFormatException ex)
            {
                System.out.println("Error: "+ex.getMessage());
                input_l_p.setBackground(Color.RED);
            }
        }

    }
}