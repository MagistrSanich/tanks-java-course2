import java.util.Date;

//Спавн, т.е. место на карте откуда будут появляться новые танки
public class Spawns {
    int x_center,y_center;
    int count_tanks;
    int time_of_spawn;
    boolean result;
    long timer;
    Spawns(int X_Center,int Y_Center,int Count_Of_Tanks,int Time_of_spawn)
    {
        x_center=X_Center;
        y_center=Y_Center;
        count_tanks=Count_Of_Tanks;
        time_of_spawn =Time_of_spawn;
        result=false;
    }
    //Принимает решение о том можно ли создавать танк.
    public boolean createTank()
    {
        if(new Date().getTime()-timer>=time_of_spawn)
        {
            result=true;
            timer=new Date().getTime();
        }
        if(result)
        {
            if(count_tanks>0)
            {
                count_tanks--;
                result=false;
                return true;
            }
        }
        return false;
    }
}
