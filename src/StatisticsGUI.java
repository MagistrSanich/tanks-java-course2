
import javax.swing.*;
import java.awt.*;

//Интерфейс с данными по итогам боя
public class StatisticsGUI extends JFrame {
    private JLabel label_title = new JLabel
            ("Stats for battle");
    private JLabel label_1=new JLabel("The number of murders: ");
    private JLabel label_2=new JLabel("");
    private JLabel label_3=new JLabel("Number of deaths: ");
    private JLabel label_4=new JLabel("");

    public StatisticsGUI(int X, int Y, int Width, int Height)
    {
        super("Kursach: Tanks.Statistics");
        this.setBounds(X, Y, Width,Height);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Container container = this.getContentPane();

        final int INDENT=5;
        int width=Width/2-INDENT*2,height=50;
        int d_h=50,d_w=Width/2+2*INDENT,
                x=0+INDENT,y=10;


        container.setLayout(null);
        //Title
        container.add(label_title);
        label_title.setBounds(x,y,Width-INDENT/2,height);

        //Morders
        y+=d_h;
        container.add(label_1);
        label_1.setBounds(x,y,width,height);

        label_2.setText(String.valueOf(Main.morders));
        container.add(label_2);
        label_2.setBounds(x+d_w,y,width,height);

        //Kills
        y+=d_h;
        container.add(label_3);
        label_3.setBounds(x,y,width,height);

        label_4.setText(String.valueOf(Main.death));
        container.add(label_4);
        label_4.setBounds(x+d_w,y,width,height);

    }
}
