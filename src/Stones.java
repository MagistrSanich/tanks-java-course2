import java.awt.*;

//Точка на экране через которую нельзя проехать
public class Stones {
    int x,y,id_obj,size_cell;
    private int color_red,color_green,color_blue;
    //Конструктор
    Stones(int Center_x, int Center_y, int Size_Cells,int id)
    {
        x=Center_x;
        y=Center_y;
        size_cell=Size_Cells;
        id_obj=id;
        this.SetColor(0,0,200);
    }
    //Отрисовка
    public void Print(Graphics g)
    {
        g.setColor(new Color(color_red,color_green,color_blue));
        g.fillRect(x*size_cell,y*size_cell,size_cell,size_cell);
    }
    //Задать цвет
    public void SetColor(int red, int green, int blue)
    {
        color_red=red;
        color_green=green;
        color_blue = blue;
    }
    //Проверить было ли столкновение с объектом параметры которого переданы в action
    public boolean checkHit(ActionForwards action)
    {
        for(int i=action.points.size()-1;i>=0;i--)
        {
            if(action.points.get(i).X==this.x  && action.points.get(i).Y==this.y)
                return true;
        }
        return false;
    }
}