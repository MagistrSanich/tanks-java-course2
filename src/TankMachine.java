import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.Date;
//Класс - общее между танками ботов и игрока
public class TankMachine {
    int x_center,y_center;
    public EN_TRENDS trend;
    public EN_TRENDS last_trend;
    private int size_cell;
    public boolean shoot;
    int id;
    long time_shoot, time_reload;
    private int color_red,color_green,color_blue;
    ActionForwards my_action;
    //Направление движения танка
    enum EN_TRENDS
    {
        E_STAND,
        E_RIGHT,
        E_UP,
        E_LEFT,
        E_DOWN;
        public static EN_TRENDS getRandom() {
            return values()[(int) (Math.random() * values().length)];
        }
    }
    //Конструктор
    public TankMachine(int Center_X, int Center_Y, int Size_Cell, int Time_Reload, int ID)
    {
        x_center=Center_X;
        y_center=Center_Y;
        size_cell=Size_Cell;
        id=ID;

        trend = EN_TRENDS.E_STAND;
        last_trend =EN_TRENDS.E_UP;
        shoot =false;
        time_reload=Time_Reload;
        time_shoot=0;
        my_action=new ActionForwards(id,x_center,y_center);
        this.SetColor(0,255,0);
    }
    //Установить цвет танка
    public void SetColor(int red, int green, int blue)
    {
        color_red=red;
        color_green=green;
        color_blue = blue;
    }
    //Нарисовать танк
    public void Print(Graphics g)
    {
        //6 cells
        if (trend == EN_TRENDS.E_UP || (trend == EN_TRENDS.E_STAND && last_trend == EN_TRENDS.E_UP)) {
            this.printCell(x_center, y_center, g);
            this.printCell(x_center - 1, y_center, g);
            this.printCell(x_center + 1, y_center, g);
            this.printCell(x_center - 1, y_center +1, g);
            this.printCell(x_center + 1, y_center +1, g);
            this.printCell(x_center, y_center -1, g);
        }
        if (trend == EN_TRENDS.E_LEFT || (trend == EN_TRENDS.E_STAND && last_trend == EN_TRENDS.E_LEFT)) {
            this.printCell(x_center, y_center, g);
            this.printCell(x_center, y_center - 1,g );
            this.printCell(x_center, y_center + 1, g);
            this.printCell(x_center + 1, y_center + 1, g);
            this.printCell(x_center +1, y_center - 1, g);
            this.printCell(x_center -1, y_center, g);
        }
        if (trend == EN_TRENDS.E_DOWN || (trend == EN_TRENDS.E_STAND && last_trend == EN_TRENDS.E_DOWN)) {
            this.printCell(x_center, y_center, g);
            this.printCell(x_center +1, y_center, g);
            this.printCell(x_center - 1, y_center, g);
            this.printCell(x_center + 1, y_center - 1,g );
            this.printCell(x_center - 1, y_center - 1, g);
            this.printCell(x_center, y_center + 1, g);
        }
        if (trend == EN_TRENDS.E_RIGHT || (trend == EN_TRENDS.E_STAND && last_trend == EN_TRENDS.E_RIGHT)) {
            this.printCell(x_center, y_center, g);
            this.printCell(x_center, y_center - 1, g);
            this.printCell(x_center, y_center + 1, g);
            this.printCell(x_center -1, y_center + 1,g );
            this.printCell(x_center - 1, y_center - 1,g );
            this.printCell(x_center + 1, y_center, g);
        }

    }
    //Закрасить одну клетку
    private void printCell(int x, int y, Graphics g){
        g.setColor(new Color(color_red,color_green,color_blue));
        g.fillRect(x*size_cell,y*size_cell,size_cell,size_cell);
    }

    //Реализовать движение танка
    public void releseMove(boolean flag)
    {
        if(flag)
        {
            x_center=my_action.x;
            y_center=my_action.y;
            if(trend!=EN_TRENDS.E_STAND)last_trend=trend;
            //trend=EN_TRENDS.E_STAND;
        } else
        {
            my_action.x=x_center;
            my_action.y=y_center;
        }
    }
    //Стрельнуть
    public void setShoot(boolean Shoot)
    {
        if(Shoot) {
            long date = new Date().getTime();
            if(date-time_shoot>=time_reload) {
                shoot = true;
                time_shoot=date;
            }
        } else {
            shoot = false;
        }
    }
    //Вернуть значение переменной shoot
    public boolean getShoot()
    {
        return shoot;
    }
    //Вернуть направление движение танка
    public EN_TRENDS getTrend()
    {
        if( trend==EN_TRENDS.E_STAND) return last_trend;
        else return trend;
    }
    //Проверить не столкнулся ли танк с объектом action
    public boolean checkHit(ActionForwards action)
    {
        for(int i=0;i<action.points.size();i++) {
            int xx = x_center;
            int yy = y_center;
            int dist = 1;
            int fx = action.points.get(i).X;
            int fy = action.points.get(i).Y;
            //6 cells
            if ( fx == xx - dist && fy == yy || fx == xx + dist && fy == yy ||
                    fx == xx - dist && fy == yy + dist || fx == xx + dist && fy == yy + dist ||
                    fx == xx && fy == yy - dist || fx == xx - dist && fy == yy-dist ||
                    fx == xx + dist && fy == yy-dist ||fx == xx && fy == yy + dist)
                return true;
        }
        return false;
    }

}
