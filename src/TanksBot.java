import java.util.Random;

//Класс танка бота
public class TanksBot extends TankMachine{
    private int    count_step;
    private final int COUNT_STEP=15;
    int rate_of_fire; // % interval (0;1000)
    //конструктор
    public TanksBot(int Center_X, int Center_Y, int Size_Cell, int time_reload, int ID, int RateOfFire) {
        super(Center_X, Center_Y, Size_Cell, time_reload, ID);
        count_step=0;
        rate_of_fire=RateOfFire;
        SetColor(155,155,0);
    }

    //Расчет места положения после движения и проверка стрельбы.
    public void chekMove(ActionForwards action_bf)
    {
        if(count_step>=COUNT_STEP) this.reedKeyBoard();
        this.culculateShoot();
        my_action.x=x_center;
        my_action.y=y_center;
        if (trend== TankMachine.EN_TRENDS.E_RIGHT) {
            my_action.x = x_center + 1;
            my_action.y = y_center;
        }
        if (trend == TankMachine.EN_TRENDS.E_UP)
        {
            my_action.x=x_center;
            my_action.y=y_center - 1;
        }
        if (trend== TankMachine.EN_TRENDS.E_LEFT)
        {
            my_action.x=x_center-1;
            my_action.y=y_center;
        }
        if (trend== TankMachine.EN_TRENDS.E_DOWN)
        {
            my_action.x=x_center;
            my_action.y=y_center + 1;
        }
        action_bf.x=my_action.x;
        action_bf.y=my_action.y;
        action_bf.id_my=id;
        action_bf.setPoints(my_action.x,my_action.y);
        action_bf.trend=trend;
        count_step++;
    }

    //Генерация случайного направления танка
    public void reedKeyBoard()
    {
        trend = TankMachine.EN_TRENDS.getRandom();
        if(trend!= TankMachine.EN_TRENDS.E_STAND) {
            last_trend = trend;
        }
        count_step=0;
    }

    //Генерация случайно стрельбы танка
    public void culculateShoot(){
        Random random = new Random();
        int temp = random.nextInt(1000);
        if(temp<rate_of_fire){
            this.setShoot(true);
        }
    }
}
