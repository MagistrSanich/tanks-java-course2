import java.awt.event.KeyEvent;

//Класс игрока
public class TanksPlayer extends TankMachine {

    private int count_lives;
    //конструктор
    public TanksPlayer(int Center_X, int Center_Y, int Size_Cell, int time_reload, int ID, int Count_Lives) {
        super(Center_X, Center_Y, Size_Cell, time_reload, ID);
        count_lives=Count_Lives;
    }
    //Расчет движения
    public void chekMove(ActionForwards action_bf)
    {
        my_action.x=x_center;
        my_action.y=y_center;
        if (trend==EN_TRENDS.E_RIGHT) {
            my_action.x = x_center + 1;
            my_action.y = y_center;
        }
        if (trend == EN_TRENDS.E_UP)
        {
            my_action.x=x_center;
            my_action.y=y_center - 1;
        }
        if (trend==EN_TRENDS.E_LEFT)
        {
            my_action.x=x_center-1;
            my_action.y=y_center;
        }
        if (trend==EN_TRENDS.E_DOWN)
        {
            my_action.x=x_center;
            my_action.y=y_center + 1;
        }
        action_bf.x=my_action.x;
        action_bf.y=my_action.y;
        action_bf.id_my=id;
        action_bf.setPoints(my_action.x,my_action.y);
        action_bf.trend=trend;
    }
    //Считывание с клавиатуры направления движения
    public void reedKeyBoard(int key)
    {
        if(trend!=EN_TRENDS.E_STAND) last_trend=trend;
        if (key==KeyEvent.VK_UP || key==KeyEvent.VK_W) {
            trend= EN_TRENDS.E_UP;
        } else
        if (key==KeyEvent.VK_LEFT || key==KeyEvent.VK_A) {
            trend= EN_TRENDS.E_LEFT;
        }else
        if (key==KeyEvent.VK_DOWN || key==KeyEvent.VK_S)
        {
            trend= EN_TRENDS.E_DOWN;
        }else
        if (key==KeyEvent.VK_RIGHT || key==KeyEvent.VK_D) {
            trend= EN_TRENDS.E_RIGHT;
        }
        if(key==KeyEvent.VK_SPACE){
            this.setShoot(true);
        }

    }
    //Оставновить танк, если пользователь никуда не едет
    public void setTrendStand(int key)
    {

        if ((key==KeyEvent.VK_UP || key==KeyEvent.VK_W)&&trend==EN_TRENDS.E_UP) {
            trend=EN_TRENDS.E_STAND;
        } else
        if ((key==KeyEvent.VK_LEFT || key==KeyEvent.VK_A)&&trend==EN_TRENDS.E_LEFT) {
            trend=EN_TRENDS.E_STAND;
        }else
        if ((key==KeyEvent.VK_DOWN || key==KeyEvent.VK_S)&&trend==EN_TRENDS.E_DOWN)
        {
            trend=EN_TRENDS.E_STAND;
        }else
        if ((key==KeyEvent.VK_RIGHT || key==KeyEvent.VK_D)&&trend==EN_TRENDS.E_RIGHT) {
            trend=EN_TRENDS.E_STAND;
        }
    }
    //Вернуть количество жизней
    public int getLives()
    {
        return count_lives;
    }
    //Отнять жизнь
    public void decLives()
    {
        count_lives--;
    }

}
