import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class MainTest {

    Main battleField = new Main();

    @Before
    public void setUp() throws Exception {
        for (int i=1;i<10;i++)
        {
            battleField.addStone(5,i);
        }
        battleField.addBot(20,20);
        battleField.addSpawn(30,30,9);
        battleField.addPlayer(40,40);
        battleField.addWeakObject(50,50);
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void clear() {
    }

    @Test
    public void checkBarriers() {
        //Stones
        ActionForwards action = new ActionForwards();
        action.id_my=999;
        for(int i=0;i<11;i++) {
            for(int j=4;j<=6;j++) {
                action.x = j;
                action.y = i;
                action.id_other_object = -1;
                action.setPoints(action.x, action.y);
                battleField.checkBarriers(action);
                Assert.assertTrue(action.id_other_object != -1);
            }
        }
        //Bots
        for(int i=18;i<=22;i++) {
            for(int j=18;j<=22;j++) {
                action.x = j;
                action.y = i;
                action.id_other_object = -1;
                action.setPoints(action.x, action.y);
                battleField.checkBarriers(action);
                Assert.assertTrue(action.id_other_object != -1);
            }
        }
        //Spawns
        for(int i=28;i<=32;i++) {
            for(int j=28;j<=32;j++) {
                action.x = j;
                action.y = i;
                action.id_other_object = -1;
                action.setPoints(action.x, action.y);
                battleField.checkBarriers(action);
                Assert.assertTrue(action.id_other_object == -1);
            }
        }
        //Players
        for(int i=38;i<=42;i++) {
            for(int j=38;j<=42;j++) {
                action.x = j;
                action.y = i;
                action.id_other_object = -1;
                action.setPoints(action.x, action.y);
                battleField.checkBarriers(action);
                Assert.assertTrue(action.id_other_object != -1);
            }
        }
        //Weak stones
        for(int i=49;i<=51;i++) {
            for(int j=49;j<=51;j++) {
                action.x = j;
                action.y = i;
                action.id_other_object = -1;
                action.setPoints(action.x, action.y);
                battleField.checkBarriers(action);
                Assert.assertTrue(action.id_other_object != -1);
            }
        }
    }
}