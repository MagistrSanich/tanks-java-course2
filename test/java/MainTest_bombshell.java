import org.junit.After;
        import org.junit.Assert;
        import org.junit.Before;
        import org.junit.Test;

        import static org.junit.Assert.*;

public class MainTest_bombshell {
    Main battleField = new Main();
    @Before
    public void setUp() throws Exception {

    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void punish() {
        battleField.addPlayer(1,1);
        battleField.punish(battleField.players.get(0).id, BombShells.EN_TYPE_TANK.EN_GOOD);
        Assert.assertTrue(battleField.players.isEmpty());
    }

    @Test
    public void punishBomb() {
        int id =5;
        //Проверка на удаление
        battleField.bombShells.add(new BombShells(1,1,1, TankMachine.EN_TRENDS.E_RIGHT,id,
                1, BombShells.EN_TYPE_TANK.EN_EVIL));
        battleField.punishBomb(id);
        Assert.assertTrue(battleField.bombShells.isEmpty());
        battleField.bombShells.add(new BombShells(1,1,1, TankMachine.EN_TRENDS.E_RIGHT,id,
                1, BombShells.EN_TYPE_TANK.EN_GOOD));
        battleField.punishBomb(id);
        Assert.assertTrue(battleField.bombShells.isEmpty());
        //Не удаляет ли лишнего
        battleField.bombShells.add(new BombShells(1,1,1, TankMachine.EN_TRENDS.E_RIGHT,id,
                1, BombShells.EN_TYPE_TANK.EN_EVIL));
        battleField.punishBomb(id+1);
        Assert.assertTrue(!battleField.bombShells.isEmpty());
        battleField.bombShells.add(new BombShells(1,1,1, TankMachine.EN_TRENDS.E_RIGHT,id,
                1, BombShells.EN_TYPE_TANK.EN_GOOD));
        battleField.punishBomb(id+1);
        Assert.assertTrue(!battleField.bombShells.isEmpty());

    }
}